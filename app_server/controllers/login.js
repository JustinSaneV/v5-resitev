var request = require('request');
var apiURL = {
  server: "http://localhost:" + process.env.PORT
};
if (process.env.API_URL) {
  apiURL.server = process.env.API_URL;
}


module.exports.login = function(req, res, next) {
  res.render('login');
}

module.exports.checkLogin = function(req, res, next) {
    var data = req.body;
    var username = data.email;
    var password = data.password;
    
    var apiParams = {
        url: apiURL.server + "/api/user/" + username + "/" + password,
        method: 'GET',
        json: {}
    };

    request(
        apiParams,
        function(err, response, content) {
            if (content.exists) {
                res.redirect('/fribox');
	        } else {
                res.render('login', {error: true, username: username, password: password});
	        }
        }
    );
}