var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');

require('./app_api/models/db');

var login = require('./app_server/routes/login');
var fribox = require('./app_server/routes/fribox');
var indexApi = require('./app_api/routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'app_server', 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileUpload({
  //Limit to 5 MB per upload (this will just truncate at the limit!)
  //See busboy for more info...
  limits: { fileSize: 5 * 1024 * 1024 },
}))

app.use('/', login);
app.use('/api', indexApi);
app.use('/login', login);
app.use('/fribox', fribox);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  var error = err.status || 500
  res.status(error);
  res.write("Error happened: " + error);
  res.end();
});

module.exports = app;
